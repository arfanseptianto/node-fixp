const ARFAN_DEMO = true;

var FixProtocol = require('./lib/protocol.js'),
    tls = require('tls'),
    moment = require('moment');

// FIX account credentials
var host = 'h72.p.ctrader.com';
var port_price = 5201;
var port_price_ssl = 5211;
var port_trade = 5202;
var port_trade_ssl = 5212;
var username = '3424252';
var password = 'chriss1207';
var senderCompID = 'demo.ctrader.3424252';
var senderSubID = 'QUOTE';
var targetCompID = 'CSERVER';
var targetSubID = 'QUOTE';

var protocol = new FixProtocol();

var loginMessage = function(reset = false) {
    return protocol.encode({
        BeginString: 'FIX.4.4',
        BodyLength: '%l',
        MsgType: 'A',
        MsgSeqNum: protocol.seqNum(),
        SenderCompID: senderCompID,
        SenderSubID: senderSubID,
        TargetCompID: targetCompID,
        TargetSubID: targetSubID,
        SendingTime: moment().subtract(3, "hours").format("YYYYMMDD-HH:mm:ss.SSS"),
        Username: username,
        Password: password,
        EncryptMethod: 0,
        HeartBtInt: 3,  // This is the inteval for heartbeat messages, recommended value for this is 10-30 seconds. 3 is just for my demo purpose.
        ResetSeqNumFlag: (reset ? 'Y' : 'N')
    }, true);
}

var logoutMessage = function() {
    return protocol.encode({
        BeginString: 'FIX.4.4',
        BodyLength: '%l',
        MsgType: '5',
        MsgSeqNum: protocol.seqNum(),
        SenderCompID: senderCompID,
        SenderSubID: senderSubID,
        TargetCompID: targetCompID,
        TargetSubID: targetSubID,
        SendingTime: moment().subtract(3, "hours").format("YYYYMMDD-HH:mm:ss.SSS")
    }, true);
}

var beatMessage = function (testReqID) {
    return protocol.encode({
        BeginString: 'FIX.4.4',
        BodyLength: '%l',
        MsgType: 0,
        MsgSeqNum: protocol.seqNum(),
        SenderCompID: senderCompID,
        SendingTime: moment().subtract(3, "hours").format("YYYYMMDD-HH:mm:ss.SSS"),
        TargetCompID: targetCompID,
        TestReqID: testReqID
    }, true);
}

var quoteMessage = function(textSymbol = 'USDSGD', intSymbol = 73) {
    return protocol.encode({
        BeginString: 'FIX.4.4',
        BodyLength: '%l',
        MsgType: 'V',
        MsgSeqNum: protocol.seqNum(),
        SenderCompID: senderCompID,
        SenderSubID: senderSubID,
        TargetCompID: targetCompID,
        TargetSubID: targetSubID,
        SendingTime: moment().subtract(3, "hours").format("YYYYMMDD-HH:mm:ss.SSS"),
        MDReqID: textSymbol,      // 262  Text version of the Symbol(55)
        SubscriptionRequestType: 1,             // 263
        MarketDepth: 1,             // 264
        MDUpdateType: 1,             // 265
        NoRelatedSym: 1,             // 146
        Symbol: intSymbol,            // 55   73 is cTrade FIX Symbol for USDSGD
        NoMDEntryTypes: 2,             // 267
        MDEntryType: "0\x01269=1",  // 269 This will give you 269=0|269=1|, This means you want to know the buy(0) and sell(1) value
    }, true);
}

var connectionOptions = {
    secureProtocol: 'TLSv1_method',
    rejectUnauthorized: false
};

if(!ARFAN_DEMO) {
    var cleartextStream = tls.connect(port_price_ssl, host, connectionOptions, ()=>{
        cleartextStream.write(loginMessage(true));
        cleartextStream.write(quoteMessage('USDSGD', 73));
    });
    
    cleartextStream.setEncoding('utf8');
    
    cleartextStream.on('data', function (data) {
        // parse the FIX message
        var data = protocol.decode(data);

        if (data.MsgType === '0' || data.MsgType === '1') {
            // if server sent a heart beat or test request, We need to respond
            cleartextStream.write(beatMessage(data.TestReqID));
        } else {
            console.log(data);
        }

        // You can catch error or rejection by MsgType,
        // but if you are to lazy to check the MsgType (like me)
        // you can use this one.
        // Error or Rejection message usually will have a Text tag
        // that describe the error/rejection.
        if (data.Text) {
            console.error(data.Text);
        }
    });

    cleartextStream.on('end', function () {
        console.log('FIX connection closed');
        process.exit(0);
    });

    cleartextStream.on('error', function (reason) {
        console.error('FIX connection error: ' + reason);
    });
}


/**
 * Below is just some console message to make the running process easier to understand
 */
if(ARFAN_DEMO) {
    var cleartextStream = tls.connect(port_price_ssl, host, connectionOptions);
    
    cleartextStream.setEncoding('utf8');
    
    cleartextStream.on('data', function (data) {
        // parse the FIX message
        var data = protocol.decode(data);

        if (data.MsgType === '0') {
            console.log('\x1b[32m%s\x1b[0m', '[' + data.MsgSeqNum + '] We are receiving Heartbeat message from server.');
            // if server sent a heart beat or test request, We need to respond
            cleartextStream.write(beatMessage(data.TestReqID));
        } else if (data.MsgType === '1') {
            console.log('\x1b[32m%s\x1b[0m', '[' + data.MsgSeqNum + '] We are receiving Test Request message from server.');
            // if server sent a heart beat or test request, We need to respond
            cleartextStream.write(beatMessage(data.TestReqID));
        } else if (data.MsgType === '5') {
            console.log('\x1b[32m%s\x1b[0m', '[' + data.MsgSeqNum + '] Our Logout request is approved and we have successfully disconnected from the server.');
        } else if (data.MsgType === 'A') {
            console.log('\x1b[32m%s\x1b[0m', '[' + data.MsgSeqNum + '] Our Logon request is approved and we have successfully connected to server.');
        } else if (data.MsgType === 'W') {
            console.log('\x1b[32m%s\x1b[0m', '[' + data.MsgSeqNum + '] We are receiving new Market Data Updates from server.');
            console.log('\x1b[32m%s\x1b[0m', 'Symbol: ' + data.Symbol);
            console.log('\x1b[32m%s\x1b[0m', 'Buy Price: ' + data.MDEntryPx[0]);
            console.log('\x1b[32m%s\x1b[0m', 'Sell Price: ' + data.MDEntryPx[1]);
        } else {
            console.log('\x1b[32m%s\x1b[0m', '[' + data.MsgSeqNum + '] We receive message with MsgType=' + data.MsgType + ' from server.');
            console.log('\x1b[32m%s\x1b[0m', data);
        }

        if (data.Text) {
            console.log('\x1b[41m%s\x1b[0m', data.Text);
        }

        console.log('========================');
        console.log('');
    });

    cleartextStream.on('end', function () {
        console.log('\x1b[32m%s\x1b[0m', 'FIX connection closed');
        process.exit(0);
    });

    cleartextStream.on('error', function (reason) {
        console.log('\x1b[41m%s\x1b[0m', 'FIX connection error: ' + reason);
    });
}

function delay(ms) {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
}

async function start_demo() {
    console.log('\x1b[33m%s\x1b[0m', 'This little apps will show you the flow of FIX API communication model in a session.');

    await delay(700);
    console.log('\x1b[33m%s\x1b[0m', 'Lets try to connect to server by sending our Logon Message.');
    var msg = loginMessage(true);
    console.log('\x1b[36m%s\x1b[0m', 'Logon Message: ' + msg.replace(/\x01/g, '|'));
    await delay(1000);
    cleartextStream.write(msg);
    await delay(1000);
    console.log('\x1b[33m%s\x1b[0m', 'Lets wait for few seconds, so we can receive some heartbeat message from server.');

    /**
     * Some of cTrade Symbol, just for reference
     * EURUSD: 1
     * EURSGD: 22333
     * SGDJPY: 56
     * USDSGD: 73
     * GBPSGD: 74
     */
    await delay(11000);
    console.log('\x1b[33m%s\x1b[0m', 'Okay, now lets try to request Market Data for USDSGD.');
    var msg = quoteMessage('USDSGD', 73);
    console.log('\x1b[36m%s\x1b[0m', 'MDRequest Message: ' + msg.replace(/\x01/g, '|'));
    await delay(1000);
    cleartextStream.write(msg);

    await delay(5000);
    console.log('\x1b[33m%s\x1b[0m', 'Now lets try to logout from the server.');
    var msg = logoutMessage();
    console.log('\x1b[36m%s\x1b[0m', 'Logout Message: ' + msg.replace(/\x01/g, '|'));
    await delay(1000);
    cleartextStream.write(msg);
}

start_demo();
